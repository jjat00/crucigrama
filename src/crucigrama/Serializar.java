package crucigrama;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializar {
    private FileInputStream fileInput;
    private FileOutputStream fileOutput;
    private ObjectInputStream objectInput;
    private ObjectOutputStream objectOutput;
    
    public void serializarObject(Crucigrama crucigrama) {
    	
    	try {
			fileOutput = new FileOutputStream("src/objetosSerializados/crucigramaSerializado");
			objectOutput = new ObjectOutputStream(fileOutput);
			
			objectOutput.writeObject(crucigrama);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				objectOutput.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    	
    	
    }

   public Crucigrama deserializarObject() {
	   Crucigrama crucigrama = null;
	   
	   try {
		fileInput = new FileInputStream("src/objetosSerializados/crucigramaSerializado");
		objectInput = new ObjectInputStream(fileInput);
		crucigrama = (Crucigrama) objectInput.readObject();
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		try {
			objectInput.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}  
	   return crucigrama;
   }
}
