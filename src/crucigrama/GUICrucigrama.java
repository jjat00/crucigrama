package crucigrama;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class GUICrucigrama extends JFrame {

    private Container containerJuego;
    private JTextArea areaPistas;
    private JPanel panelPistas;
    private EscuchaLabels escuchaLabels;
    private EscuchaKeys escuchaKeys;
    private Archivo archivo;
    private int columnaClicked;
    private int filaClicked;
    private Border border;
    private JLabel tablero[][];
    private Crucigrama crucigrama;
    private int numeroFilas;
    private int numeroColumnas;
    
    public GUICrucigrama() {
        super("Crucigrama Game");
        inicializarVentanaJuego();
        // Set default window configuration
        this.setUndecorated(false);
        this.setPreferredSize(new Dimension(1000, 600));
        this.pack();
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void inicializarVentanaJuego() {
        // JFrame container zona de Juego anda layout
        containerJuego = getContentPane();
        containerJuego.setLayout(new BorderLayout());


        // create listener object and logic control
        escuchaLabels = new EscuchaLabels();
        escuchaKeys = new EscuchaKeys();

        //create JComponent
        archivo = new Archivo();
        panelPistas = new JPanel();
        panelPistas.setBackground(Color.WHITE);
        border = BorderFactory.createLineBorder(Color.BLACK, 1, true);

        areaPistas = new JTextArea(10,10);
        //areaPistas.setText(archivo.leerArchivo("src/recursos/pistas.txt"));
        areaPistas.setEditable(false);
        panelPistas.add(areaPistas);

        numeroFilas = 20;
        numeroColumnas = 20;

        
        crucigrama = new Crucigrama(numeroFilas, numeroColumnas);
        tablero = crucigrama.obtenerTablero();
        crucigrama.componentesJPanelTablero();
        initCrucigrama();


        containerJuego.add(crucigrama, BorderLayout.CENTER);
        containerJuego.add(panelPistas, BorderLayout.EAST);
    }

    public void initCrucigrama() {
        addEscuchaLabel();
        rellenarEtiquetas(2, 4, "1");
        rellenarEtiquetas(6, 1, "2");
        rellenarEtiquetas(7, 10, "3");
        rellenarEtiquetas(8, 3, "4");
        rellenarEtiquetas(12, 12, "5");
        rellenarEtiquetas(14, 5, "6");
        rellenarEtiquetas(1, 15, "7");
        rellenarEtiquetas(18, 10, "8");
        rellenarEtiquetas(1, 18, "9");
        rellenarEtiquetas(4, 12, "10");
        rellenarEspaciosPalabra(3, 4, 6, 1);
        rellenarEspaciosPalabra(8, 10, 8, 1);
        rellenarEspaciosPalabra(13, 12, 6, 1);
        rellenarEspaciosPalabra(2, 15, 8, 1);
        rellenarEspaciosPalabra(2, 18, 6, 1);
        rellenarEspaciosPalabra(6, 2, 5, 0);
        rellenarEspaciosPalabra(8, 4, 12, 0);
        rellenarEspaciosPalabra(14, 6, 7, 0);
        rellenarEspaciosPalabra(18, 11, 7, 0);
        rellenarEspaciosPalabra(4, 13, 6, 0);
    }

    public void rellenarEspaciosPalabra(int filaInicio, int columnaInicio, int longitud, int sentido) {
        //sentido 0 para horizontal  y 1 para vertical 
        if (sentido == 0) {
            for (int columna = 0; columna < longitud; columna++) {
                    //tablero[filaInicio][columnaInicio+columna].setText(String.valueOf(filaInicio)+String.valueOf(columnaInicio+columna));
                    tablero[filaInicio][columnaInicio+columna].setBorder(border);
                    tablero[filaInicio][columnaInicio+columna].addMouseListener(escuchaLabels);
            }
        }
        if (sentido == 1) {
            for(int fila= 0; fila < longitud; fila++ ){
                //tablero[filaInicio+fila][columnaInicio].setText(String.valueOf(filaInicio+fila)+String.valueOf(columnaInicio));
                tablero[filaInicio+fila][columnaInicio].setBorder(border);
                tablero[filaInicio+fila][columnaInicio].addMouseListener(escuchaLabels);
            }
        }
    }

    public void rellenarEtiquetas(int filaInicio, int columnaInicio, String string) {
        for (int fila = 0; fila < numeroFilas; fila++) {
            for (int columna = 0; columna < numeroColumnas ; columna++) {
                tablero[filaInicio][columnaInicio].setText(string);
            }
        }
    }



    public void addEscuchaLabel() {

        for (int fila = 0; fila < numeroFilas; fila++) {
            for (int columna = 0; columna < numeroColumnas; columna++) {
                tablero[fila][columna].addKeyListener(escuchaKeys);
            }
        }
    }


    private class EscuchaLabels extends MouseAdapter {
        public void mouseClicked(MouseEvent event) {
            for (int fila = 0; fila < numeroFilas; fila++) {
                for (int columna = 0; columna < numeroColumnas; columna++) {
                    if (event.getSource() == tablero[fila][columna]) {
                        filaClicked = fila;
                        columnaClicked = columna;
                        System.out.print(filaClicked);
                        System.out.println(columnaClicked);
                    }
                }
            }
        }
    }

    private class EscuchaKeys implements KeyListener {
        public void keyPressed(KeyEvent arg0) {}
        public void keyReleased(KeyEvent arg0) {}

        public void keyTyped(KeyEvent event) {
            crucigrama.setLetra(String.valueOf(event.getKeyChar()).toUpperCase(), filaClicked, columnaClicked); 
            System.out.println(event.getKeyChar());
        }
    }
}