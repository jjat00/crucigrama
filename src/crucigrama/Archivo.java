package crucigrama;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;

import javax.swing.JFileChooser;


public class Archivo {

 /** The file read. */
 private FileReader fileRead;
    
 /** The input. */
 private BufferedReader input;
 
 /** The file writer. */
 private FileWriter fileWriter;
 
 /** The output. */
 private BufferedWriter output;
 
 /** The chooser. */
 private JFileChooser chooser;
 
 /** The ruta archivo. */
 private File rutaArchivo;

 /**
  * Seleccionar directorio.
  * Abre una ventana en donde el usuario escoge la ruta para guardar
  * un archivo.
  */
 public void seleccionarDirectorio() {
     chooser = new JFileChooser();
     chooser.setCurrentDirectory(new java.io.File("."));
     chooser.setDialogTitle("Escoger ruta para guardar archivo");    
     if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
         System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
         rutaArchivo = chooser.getSelectedFile();          
         System.out.println("getSelectedFile() : " + rutaArchivo);
     } else {
         System.out.println("No Selection ");
     }
 }

 /**
  * Leer archivo.
  * lee un archivo guardado en la ruta especificada.
  * @return the string
  */
 public String[] leerArchivo(String pathFile){
	 int numeroPalabras = 6;
    String []texto = new String[numeroPalabras];
    try {
        fileRead = new FileReader(pathFile);
        input = new BufferedReader(fileRead);
        String linea  = input.readLine();
        int cualLinea = 0;
        int longitudLinea = 0;
        while (cualLinea < numeroPalabras) {
            linea = input.readLine();
            longitudLinea = linea.split(" ").length;
            String respuesta = linea.split(" ")[longitudLinea-1];
            texto[cualLinea] = respuesta  ;
            cualLinea++;
        }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    return texto;
 }

 /**
  * Escribir archivo.
  * Crea un archivo de texto y escribe una linea en la ruta escogida
  * por el usuario.
  * @param linea the linea
  */
 public void escribirArchivo(String linea) {
     try {
         fileWriter = new FileWriter(rutaArchivo, true);
     } catch (IOException e) {
         e.printStackTrace();
     }
     output = new BufferedWriter(fileWriter);
     try {
         output.write(linea);
     } catch (IOException e) {
         e.printStackTrace();
     }
     try {
         output.newLine();
     } catch (IOException e) {
         e.printStackTrace();
     }finally{
         try {
             output.close();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
 }
}
