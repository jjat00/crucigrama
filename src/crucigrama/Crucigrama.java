package crucigrama;

import java.io.Serializable;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Color;


public class Crucigrama extends JPanel implements Serializable{

    private Border border;
    private int numeroFilas;
    private int numeroColumnas;
    private JLabel tablero[][];

    public Crucigrama(int filas, int columnas ){
        
        numeroFilas = filas;
        numeroColumnas = columnas;
        border = BorderFactory.createLineBorder(Color.BLACK, 1, true);
        tablero = new JLabel[numeroFilas][numeroColumnas];
        this.setLayout(new GridLayout(numeroFilas, numeroColumnas));
        this.setBackground(Color.WHITE);
        this.setBorder(border);
    }


    public void componentesJPanelTablero() {

        for (int fila = 0; fila < numeroFilas; fila++) {
            for (int columna = 0; columna < numeroColumnas; columna++) {
                tablero[fila][columna] = new JLabel();
                tablero[fila][columna].setOpaque(true);
                tablero[fila][columna].setFocusable(true);
                tablero[fila][columna].setHorizontalAlignment(JLabel.CENTER);
                tablero[fila][columna].setVerticalAlignment(JLabel.CENTER);
                tablero[fila][columna].setBackground(Color.WHITE);
                //tablero[fila][columna].addKeyListener(escuchaKeys);
                this.add(tablero[fila][columna]);
            }
        }
    }
    

    public void setLetra(String letra, int fila, int columna) {
        tablero[fila][columna].setText(letra);
    }

    public JLabel[][] obtenerTablero() {
        return tablero;
    }
}
