package crucigrama;

import java.awt.EventQueue;
import javax.swing.UIManager;

public class Principal {

	/**
	 * The main method.
	 * M�todo principal que ejecuta todo el programa.
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
            String className = UIManager.getCrossPlatformLookAndFeelClassName();
            UIManager.setLookAndFeel(className);
        } catch (Exception e) {}

		EventQueue.invokeLater(new Runnable(){
        public void run() {
				GUICrucigrama gui = new GUICrucigrama();
            }
        });
	}

}
